using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveForward : MonoBehaviour
{
    public float velocidad = 5f;
    public Vector3 escala = new Vector3(1f, 2f, 3f);
    void Start()
    {
        transform.localScale = escala;
    }

    void Update()
    {
        transform.position += Vector3.left * velocidad * Time.deltaTime;
    }

}

