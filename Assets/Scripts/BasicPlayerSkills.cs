using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BasicPlayerSkills : MonoBehaviour
{
    public int vida = 100;
    public float velocidad = 2f;
    public Vector3 direccion = new Vector3(-2f, 0, 0);
    void Start()
    {
        Curar();
        Debug.Log("vida actual: "+vida);
        Curar();
        Debug.Log("vida actual: "+vida);
        RecibirDanio();
        Debug.Log("vida actual: "+vida);
    }

    void Update()
    {
        Mover();
        Debug.Log("Posición actual: "+transform.position);
    }

    private void Mover()
    {
        transform.position += direccion * velocidad * Time.deltaTime;
    }

    private void  Curar()
    {
        Debug.Log("CURAR!");
        vida += 15;
    }

    private void RecibirDanio()
    {
        Debug.Log("Recibir Daño");
        vida -= 10;
    }

}

